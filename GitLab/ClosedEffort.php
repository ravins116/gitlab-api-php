<?php

namespace GitLab;

class ClosedEffort {
	private $userId;

	private $dateFrom;

	private $dateTo;

	private $projectId;

	private $projectsUrl;

	private $responsePage;

	private $responseTotalPages;


	public function __construct(?string $userId, string $dateFrom, string $dateTo, string $projectId) {
		$this->userId = $userId;
		$this->dateFrom = $dateFrom;
		$this->dateTo = $dateTo;
		$this->projectId = $projectId;
		$this->projectsUrl = 'https://gitlab.com/api/v4/projects/';
		$this->responsePage = 1;
		$this->responseTotalPages = null;
	}

	public function sumClosedEffort() {
		$data = $this->curlData();
		if(is_array($data)){
			$result = $this->sumEffortForIssuesItems($data['response']);
			if ($this->responsePage <= $this->responseTotalPages) {
				$data = $this->curlData();
				$result += $this->sumEffortForIssuesItems($data['response']);
			}
		} else {
			$result = '';
			//todo add error msg. something wrong.
		}
		return $result;
	}

	private function curlData() {
		$transferData = new TransferData($this->issuesUrl(), $this->responsePage);
		$data = $transferData->curlWithHeaderData();
		$this->responsePage = $data['responsePage'];
		$this->responseTotalPages = $data['responseTotalPages'];

		return $data;
	}

	/**
	 * https://docs.gitlab.com/ee/api/issues.html
	 **/
	private function issuesUrl() {
		$params = !empty($this->userId) ? '&assignee_id=' . (string)$this->userId : '';
		$params .= !empty($this->dateFrom) ? '&created_after=' . (string)$this->dateFrom : '';
		$params .= !empty($this->dateTo) ? '&created_before=' . (string)$this->dateTo : '';
		$params .= !empty($this->responsePage) ? '&page=' . $this->responsePage : '';
		return $this->projectsUrl . (string)$this->projectId . '/issues?state=closed' . $params . '&scope=all&per_page=100';
	}

	private function sumEffortForIssuesItems(array $items) {
		$effort = 0;
		foreach ($items as $issueItem) {
			$effort += $issueItem->time_stats->time_estimate;
		}
		return $this->secToHours($effort);
	}

	private function secToHours(int $sec) {
		return $sec / 3600;
	}
}