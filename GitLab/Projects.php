<?php

namespace GitLab;
include_once('Project.php');
include_once('Configuration.php');

class Projects {
	private $group;

	private $userId;

	public function __construct(string $group = null, string $userId = null) {
		$this->group = $group;
		$this->userId = $userId;
	}

	public function data(): array {
		$projects = [];
		$transferData = new TransferData($this->url());
		$data = $transferData->curl();
		foreach ($data as $project) {
			$projects[] = new Project($project);
		}
		return $projects;
	}

	private function url(): string {
		$groupProjects = $this->groupProjectsUrl();
		return ($groupProjects) ? $groupProjects : $this->userProjectsUrl();
	}

	private function groupProjectsUrl(): string {
		$params = !empty($this->group) ? $this->group : '';
		return 'https://gitlab.com/api/v4/groups/' . $params . '/projects/';
	}

	private function userProjectsUrl(): string {
		$params = !empty($this->userId) ? $this->userId : '';
		return 'https://gitlab.com/api/v4/user/' . $params . '/projects/';
	}


}