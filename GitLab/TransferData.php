<?php

namespace GitLab;
include_once(__DIR__ . '/../log4php/Logger.php');

class TransferData {
	private $log;

	private $url;

	private $responsePage;

	private $responseTotalPages;

	public function __construct(string $url, int $responsePage = null) {
		$this->url = $url;
		$this->responsePage = $responsePage;
		$this->responseTotalPages = null;
		$this->log = \Logger::getLogger('main');
		\Logger::configure(__DIR__ . '/../log4php.xml');
	}

	public function curl(): array {
		$ch = curl_init();
		$this->curlOptions($ch);

		$response = curl_exec($ch);
		if (curl_error($ch) === false) {
			$response = curl_error($ch);
		}
		curl_close($ch);
		$this->curlErrors($response);
		if (is_object(json_decode($response))) $this->log->error($response);
		if (empty($response)) {
			$response = array();
		} else {
			$response = json_decode($response);
		}
		return $response;
	}

	private function curlOptions($ch) {
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		$this->options($ch);

		return $ch;
	}

	private function options($ch): void {
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('PRIVATE-TOKEN: ' . Configuration::TOKEN, 'Content-Type: application/json'));
		curl_setopt($ch, CURLOPT_URL, $this->url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	}

	private function curlErrors($response): void {
		if (Configuration::TOKEN == 'TEST') $this->log->info('Set correct GitLab Token');
		if (is_null(json_decode($response))) $this->log->error('Missing CURLOPT_URL parameters or go for GitLab API support');
	}

	public function curlWithHeaderData() {
		$ch = curl_init();
		$this->curlOptions($ch);
		curl_setopt($ch, CURLOPT_HEADER, 1);

		$response = curl_exec($ch);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$header = substr($response, 0, $header_size);
		$result = substr($response, $header_size);
		if (curl_error($ch) === false) {
			$result = curl_error($ch);
		}
		curl_close($ch);
		$this->responsePage += 1;
		if (!$this->responseTotalPages) {
			$headerParams = $this->curlHeaders($header);
			$this->responseTotalPages = (int)$headerParams['X-Total-Pages'];
		}
		$data = [
			'response' => json_decode($result),
			'responsePage' => $this->responsePage,
			'responseTotalPages' => $this->responseTotalPages
		];

		return $data;
	}

	private function curlHeaders(string $output) {
		$headers = array();
		$data = explode("\n", $output);
		$headers['status'] = $data[0];
		array_shift($data);
		foreach ($data as $part) {
			$middle = explode(':', $part);
			if (trim($middle[0]) && trim($middle[1])) {
				$headers[trim($middle[0])] = trim($middle[1]);
			}
		}
		return $headers;
	}

	public function curlPost() {
		$ch = curl_init();
		$this->curlPostOptions($ch);

		$response = curl_exec($ch);
		if (curl_error($ch) === false) {
			$response = curl_error($ch);
		}
		curl_close($ch);
		$this->curlErrors($response);
		return json_decode($response);
	}

	private function curlPostOptions($ch) {
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		$this->options($ch);
		return $ch;
	}
}