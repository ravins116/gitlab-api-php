<?php

namespace GitLab;

class Group {
	private $id;

	private $name;

	public function __construct($group) {
		$this->id = $group->id;
		$this->name = $group->name;
	}

	public function id(): string {
		return $this->id;
	}

	public function name(): string {
		return $this->name;
	}
}