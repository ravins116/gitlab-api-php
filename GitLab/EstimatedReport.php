<?php

namespace GitLab;

use Logger;

include_once(__DIR__ . '/../log4php/Logger.php');

class EstimatedReport {
	private $userName;

	private $userEmail;

	private $projectName;

	private $dateFrom;

	private $dateTo;

	private $group;

	private $log;

	private $gitLab;

	public function __construct(?string $userName, ?string $userEmail, string $projectName, string $dateFrom, string $dateTo) {
		$this->userName = $userName;
		$this->userEmail = $userEmail;
		$this->projectName = $projectName;
		$this->dateFrom = $dateFrom;
		$this->dateTo = $dateTo;
		$this->group = null;
		$this->log = Logger::getLogger('main');
		Logger::configure(__DIR__ . '/../log4php.xml');
		$this->gitLab = new Gitlab($this->projectName, '', $this->userName, $this->userEmail);

	}

	public function hours(): float {
		$this->group = $this->gitLab->getGroup();
		$projectId = $this->gitLab->getProjectId();
		$userId = $this->gitLab->userId();
		$report = new ClosedEffort($userId, $this->dateFrom, $this->dateTo, $projectId);
		return $report->sumClosedEffort();
	}

}