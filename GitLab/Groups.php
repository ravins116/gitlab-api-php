<?php

namespace GitLab;

include_once('Group.php');

class Groups {
	private $projectName;

	private $groupsUrl;

	public function __construct(string $projectName) {
		$this->projectName = $projectName;
		$this->groupsUrl = 'https://gitlab.com/api/v4/groups/';
	}

	public function data(): array {
		$groups = [];
		$transferData = new TransferData($this->url());
		$data = $transferData->curl();
		foreach ($data as $group) {
			$groups[] = new Group($group);
		}
		return $groups;
	}

	/**
	 * https://docs.gitlab.com/ee/api/groups.html
	 **/
	private function url(): string {
		return $this->groupsUrl;
	}
}