<?php

namespace GitLab;

use Logger;

include_once(__DIR__ . '/../log4php/Logger.php');

class Gitlab {
	private $projectName;

	private $userName;

	private $userEmail;

	private $log;

	private $group;

	private $projectId;

	private $groupName;

	public function __construct(string $projectName, ?string $groupName, ?string $userName, ?string $userEmail) {
		$this->log = Logger::getLogger('main');
		Logger::configure(__DIR__ . '/../log4php.xml');
		$this->userName = $userName;
		$this->userEmail = $userEmail;
		$this->projectName = $this->projectNameModified($projectName);
		$this->groupName = $groupName;
		$this->group = $this->setGroup();
		$this->projectId = $this->getProjectId();

	}

	private function projectNameModified($projectName): string {
		return mb_strtolower($projectName, 'UTF-8');
	}

	private function setGroup() {
		if (empty($this->projectName) && !empty($this->groupName)) {
			$groupId = $this->groupId();
		} else {
			$groupId = $this->projectGroupId();
		}
		return $groupId;
	}

	private function groupId(): string {
		$groups = $this->groups();
		$id = $this->id($groups, $this->groupName);
		if (!$id) {
			$this->log->info('Gitlab: Empty result: Please set correct Gitlab Group name or you do not have access to : "' . (string)$this->groupName . '"');
			die();
		}
		return $id;
	}

	private function groups(): array {
		$GitlabGroups = new Groups($this->projectName);
		return $GitlabGroups->data();
	}

	public function id($items, $name): ?string {
		$id = null;
		foreach ($items as $item) {
			$itemName = $item->name();
			if ($itemName === $name) {
				$id = $item->id();
				break;
			}
		}
		$this->log->debug('Is there id (?)--> ' . $id);
		return $id;
	}

	private function projectGroupId(): string {
		$id = null;
		$projects = null;
		$groups = $this->groups();
		foreach ($groups as $group) {
			$GitlabProjects = new Projects($group->id());
			$projects = $GitlabProjects->data();
			if ($this->id($projects, $this->projectName)) {
				$id = $group->id();
			}
		}
		if (!$id) $this->log->info('Gitlab: Empty result: Please set correct project name or you do not have access to : "' . (string)$this->projectName . '"');
		return $id;
	}

	/**
	 * @return string or Null
	 */
	public function getProjectId(): ?string {
		return (!empty($this->projectName)) ? $this->projectId() : null;
	}

	private function projectId(): ?string {
		$GitlabProjects = new Projects($this->group);
		$projects = $GitlabProjects->data();
		$id = $this->id($projects, $this->projectName);
		if (!$id) $this->log->info('Gitlab: Empty result: Please set correct project name');
		return $id;
	}

	public function userId(): ?string {
		$GitlabUsers = new Users($this->userEmail);
		$users = $GitlabUsers->data();
		$id = $this->id($users, $this->userName);
		if (!$id) $this->log->debug('Gitlab: Try to get project hours');
		return $id;
	}

	/**
	 * @return string
	 */
	public function getGroup(): string {
		return $this->group;
	}
}