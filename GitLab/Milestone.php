<?php

namespace GitLab;

class Milestone {
	private $id;

	private $iid;

	private $title;

	private $description;

	private $state;

	private $web_url;

	public function __construct($milestone) {
		$this->id = $milestone->id;
		$this->iid = $milestone->iid;
		$this->title = $milestone->title;
		$this->description = $milestone->description;
		$this->state = $milestone->state;
		$this->web_url = $milestone->web_url;

	}

	public function iid(): string {
		return $this->iid;
	}

	public function id(): string {
		return $this->id;
	}

	public function name(): string {
		return $this->title;
	}
}