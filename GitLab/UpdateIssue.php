<?php
/**
 * Class updates an existing project issue. This call can also used to mark an issue as closed.
 * PUT /projects/:id/issues/:issue_iid
 */

namespace GitLab;

use Logger;

include_once(__DIR__ . '/../log4php/Logger.php');

class UpdateIssue {
	private $issueId;

	private $group;

	private $log;

	private $projectId;

	private $issueTitle;

	private $issueTimeSpent;

	public function __construct(string $issueId, string $issueTitle, string $projectId, string $issueTimeSpent = null) {
		$this->issueId = $issueId;
		$this->group = null;
		$this->log = Logger::getLogger('main');
		Logger::configure(__DIR__ . '/../log4php.xml');
		$this->projectId = $projectId;
		$this->issueTitle = $issueTitle;
		$this->issueTimeSpent = $issueTimeSpent;
	}

	public function update(): bool {
		$done = false;
		$transferData = new TransferData($this->issueUrl());
		$data = $transferData->curlPost();

		if (property_exists('data', 'title')) {
			$done = $this->updateNotification($done, $data);
		} else {
			if (isset($data->error) && !empty($data->error)) {
				$this->log->error('Gitlab: Issue Update Error :  ' . $data->error . PHP_EOL);
				$done = false;
			} else {
				$done = true;
			}
		}
		return $done;
	}

	private function issueUrl(): string {
		if (is_null($this->issueTimeSpent)) {
			//remove all spend time
			$url = 'https://gitlab.com/api/v4/projects/'
				. (string)$this->projectId . '/issues/' . (string)$this->issueId
				. '/reset_spent_time';

		} elseif ($this->issueTimeSpent) {
			//set spend time
			$url = 'https://gitlab.com/api/v4/projects/'
				. (string)$this->projectId . '/issues/' . (string)$this->issueId
				. '/add_spent_time?duration=' . (string)$this->issueTimeSpent . 'h';

		} else {
			//change title
			$newIssueTitle = $this->issueTitle; //todo: custom functions to change title
			$url = 'https://gitlab.com/api/v4/projects/'
				. (string)$this->projectId . '/issues/' . (string)$this->issueId
				. '?title=' . rawurlencode($newIssueTitle);

		}
		return $url;
	}

	private function updateNotification($done, $data) {
		if ($this->issueTimeSpent) {
			$this->log->info('Gitlab: Work Effort updated: ' . $data->title . PHP_EOL);
			$done = true;
		} else {
			$this->log->error('Gitlab: Issue Update Error :  ' . $data->title . PHP_EOL);
		}

		return $done;
	}

}