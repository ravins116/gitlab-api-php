<?php

namespace GitLab;

use Logger;

include_once(__DIR__ . '/../log4php/Logger.php');

class MilestoneIssues {
	private $projectName;

	private $milestoneTitle;

	private $log;

	private $businessLabels;

	private $projectIssues;

	private $gitlabGroupTitle;

	public function __construct(string $projectName, string $milestoneTitle, string $gitlabGroupTitle, bool $projectIssues = null) {
		$this->projectName = $projectName;
		$this->milestoneTitle = $milestoneTitle;
		$this->log = Logger::getLogger('main');
		Logger::configure(__DIR__ . '/../log4php.xml');
		$this->businessLabels = Configuration::ISSUES_BUSINESS_LABELS;
		$this->projectIssues = $projectIssues;
		$this->gitlabGroupTitle = $gitlabGroupTitle;

	}

	public function updateIssueTimeSpent($taskHours): void {
		$businessIssues = $this->businessIssues();
		foreach ($businessIssues as $issue) {
			foreach ($issue as $taskId => $params) {
				$businessValue = $params[2];
				$businessValueInSeconds = $businessValue / 3600;
				$projectId = $params[1];
				$taskTitle = $params[0];

				if ((int)$taskHours != (int)$businessValueInSeconds) {
					$this->removeTimeSpent($taskId, $taskTitle, $projectId);
					$this->addTimeSpent($taskId, $taskTitle, $projectId, $taskHours);
				}
			}
		}
		echo "Gitlab Work Effort updated";
	}

	public function businessIssues(): array {
		$issues = [];
		$workItems = $this->milestoneIssues();
		foreach ($workItems as $issue) {
			$issues[] = $this->businessIssue($issue);
		}

		foreach ($issues as $key => $value) {
			if (empty($value)) {
				unset($issues[$key]);
			}
		}

		if (!$workItems || !$issues) {
			$this->log->error('Gitlab: Empty result: Milestone business issues with labels (' . implode(',', $this->businessLabels) . ') not found');
			die;
		}

		return $issues;
	}

	public function milestoneIssues(): array {
		$issues = [];
		$transferData = new TransferData($this->url());
		$data = $transferData->curl();
		if ($data) {
			foreach ($data as $issue) {
				$issues[] = json_decode(json_encode($issue), True);
			}
		}
		if (!$data || !$issues) {
			$this->log->info('Gitlab: Empty result: Milestone ( ' . (string)$this->milestoneTitle . ' ) issues not found');
			die;
		}

		return $issues;
	}

	/**
	 * Get all issues assigned to a single parent milestone
	 * GET /groups/:id/milestones/:milestone_id/issues || GET /projects/:id/milestones/:milestone_id/issues
	 */
	private function url(): string {

		$milestones = new Milestones($this->projectName, $this->milestoneTitle, $this->gitlabGroupTitle, $this->projectIssues);
		$milestoneParentId = 'groups/' . $milestones->groupId();

		if ($this->projectIssues) {
			$milestoneParentId = 'projects/' . $milestones->projectId();
		}

		return 'https://gitlab.com/api/v4/' . $milestoneParentId . '/milestones/' . $milestones->milestoneId() . '/issues';
	}

	private function businessIssue($issue): array {
		$i = array();
		foreach ($issue['labels'] as $lbl) {
			if (in_array($lbl, $this->businessLabels)) {
				$i[$issue['iid']] = array($issue['title'], $issue['project_id'], $issue['time_stats']['time_estimate']);
			}
		}
		return $i;
	}

	private function removeTimeSpent($taskId, $taskTitle, $projectId) {
		$issue = new UpdateIssue((string)$taskId, $taskTitle, $projectId);
		$allDone = $issue->update();
		if ($allDone) {
			$this->log->info('Gitlab: "' . (string)$taskTitle . '" -  REMOVE spent time');
		}
	}

	private function addTimeSpent($taskId, $taskTitle, $projectId, $spentHours) {
		$issue = new UpdateIssue((string)$taskId, $taskTitle, $projectId, (string)$spentHours);
		$allDone = $issue->update();
		if ($allDone) {
			$this->log->info('Gitlab: "' . (string)$taskTitle . '" -  ADD spent time');
		}
	}
}