<?php

namespace GitLab;

class Project {
	private $id;

	private $name;

	public function __construct($project) {
		$this->id = $project->id;
		$this->name = $project->name;
	}

	public function name(): string {
		return $this->name;
	}

	public function Id(): string {
		return $this->id;
	}
}