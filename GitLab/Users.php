<?php

namespace GitLab;

include_once('User.php');

class Users {
	private $userName;

	private $email;

	private $usersUrl;

	public function __construct(string $email = null, string $userName = null) {
		$this->userName = $userName;
		$this->email = $email;
		$this->usersUrl = 'https://gitlab.com/api/v4/users/';
	}

	public function data(): array {
		$users = [];
		$transferData = new TransferData($this->url());
		$data = $transferData->curl();
		foreach ($data as $user) {
			$users[] = new User($user);
		}
		return $users;
	}

	/**
	 * https://docs.gitlab.com/ee/api/users.html
	 **/
	private function url(): string {
		$params = !empty($this->userName) ? '&username=' . (string)$this->userName : '';
		$params .= !empty($this->email) ? '&search=' . (string)$this->email : '';
		return $this->usersUrl . '?active=true' . $params;
	}
}