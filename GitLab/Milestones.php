<?php

namespace GitLab;

use Logger;

include_once(__DIR__ . '/../log4php/Logger.php');

class Milestones {
	private $milestoneTitle;

	private $log;

	private $projectName;

	private $gitLab;

	private $groupId;

	private $projectIssues;

	private $groupName;

	public function __construct(string $projectName, string $milestoneTitle, string $groupName = null, bool $projectIssues = null) {
		$this->milestoneTitle = $milestoneTitle;
		$this->log = Logger::getLogger('main');
		Logger::configure(__DIR__ . '/../log4php.xml');
		$this->projectName = $projectName;
		$this->groupName = $groupName;
		$this->gitLab = new Gitlab($this->projectName, $this->groupName, null, null);
		$this->groupId = $this->gitLab->getGroup();
		$this->projectIssues = $projectIssues;
	}

	public function milestoneId(): string {
		$milestones = $this->data();
		$id = $this->gitLab->id($milestones, $this->milestoneTitle);
		if (!$id) {
			$this->log->info('Gitlab: Empty result: Please set correct milestone title');
			die;
		}
		return $id;
	}

	private function data(): array {
		$milestones = [];
		$transferData = new TransferData($this->url());
		$data = $transferData->curl();
		foreach ($data as $item) {
			$milestones[] = new Milestone($item);
		}
		return $milestones;
	}

	private function url() {
		return ($this->projectIssues) ? $this->projectUrl() : $this->groupUrl();
	}

	/**
	 * Gets all issues assigned to a single project milestone.
	 * GET /projects/:id/milestones/
	 */
	private function projectUrl(): string {
		return 'https://gitlab.com/api/v4/projects/' . $this->projectId() . '/milestones/';
	}

	public function projectId(): string {
		return $this->gitLab->getProjectId();
	}

	/**
	 * Gets all issues assigned to a single group milestone.
	 * GET /groups/:id/milestones/
	 */
	private function groupUrl(): string {
		if ($this->groupId === null) {
			$this->groupId = $this->gitLab->getGroup();
		}
		return 'https://gitlab.com/api/v4/groups/' . $this->groupId . '/milestones/';
	}

	public function groupId(): string {
		return $this->groupId;
	}


}