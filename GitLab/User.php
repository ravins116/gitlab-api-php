<?php

namespace GitLab;

class User {
	private $id;

	private $userName;

	private $name;

	public function __construct($user) {
		$this->id = $user->id;
		$this->userName = $user->username;
		$this->name = $user->name;
	}

	public function name(): string {
		return $this->name;
	}

	public function userName(): string {
		return $this->userName;
	}

	public function Id(): string {
		return $this->id;
	}
}