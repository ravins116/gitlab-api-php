<?php

use GitLab\EstimatedReport;
use GitLab\MilestoneIssues;
use GitLab\Milestones;

include_once __DIR__ . '/../GitLab/Configuration.php';
include_once __DIR__ . '/../GitLab/Gitlab.php';
include_once __DIR__ . '/../GitLab/Users.php';
include_once __DIR__ . '/../GitLab/User.php';
include_once __DIR__ . '/../GitLab/Projects.php';
include_once __DIR__ . '/../GitLab/EstimatedReport.php';
include_once __DIR__ . '/../GitLab/Groups.php';
include_once __DIR__ . '/../GitLab/ClosedEffort.php';
include_once __DIR__ . '/../GitLab/TransferData.php';
include_once __DIR__ . '/../GitLab/MilestoneIssues.php';
include_once __DIR__ . '/../GitLab/Milestones.php';
include_once __DIR__ . '/../GitLab/Milestone.php';

class GitLabProjectReportTest extends \PHPUnit\Framework\TestCase {
	public function testProjectHoursForProgrammer() {
		$projectName = 'sample-landpage-bootstrap-v3.3.6';
		$dateFrom = '2018-01-01';
		$dateTo = '2018-12-30';
		$employee = 'Artūrs Rāviņš';
		$employeeEmail = 'ravins.arturs@gmail.com';

		$report = new EstimatedReport($employee, $employeeEmail, $projectName, $dateFrom, $dateTo);
		$hours = $report->hours();
		$this->assertEquals(6.75, $hours);
	}

	public function testAllProjectsHoursForProgrammer() {
		$projectsNames = array('sample-landpage-bootstrap-v3.3.6');
		$dateFrom = '2018-01-01';
		$dateTo = '2018-12-30';
		$employee = 'Artūrs Rāviņš';
		$employeeEmail = 'ravins.arturs@gmail.com';
		$hours = 0;
		if (count(array_unique($projectsNames)) === count($projectsNames)) {
			foreach ($projectsNames as $projectName) {
				$report = new EstimatedReport($employee, $employeeEmail, $projectName, $dateFrom, $dateTo);
				$hours += $report->hours();
			}
		} else {
			echo '---> You have some project more than one time in "projects names" array!';
		}
		$this->assertEquals(6.75, $hours);
	}


	public function testAllProjectHours() {
		$projectName = 'sample-landpage-bootstrap-v3.3.6';
		$dateFrom = '2018-01-01';
		$dateTo = '2018-12-30';
		$employee = '';
		$employeeEmail = '';

		$report = new EstimatedReport($employee, $employeeEmail, $projectName, $dateFrom, $dateTo);
		$hours = $report->hours();
		$this->assertEquals(6.75, $hours);
	}

	public function testAllProjectsHours() {
		$projectsNames = array('sample-landpage-bootstrap-v3.3.6');
		$dateFrom = '2018-01-01';
		$dateTo = '2018-12-30';
		$employee = '';
		$employeeEmail = '';
		$hours = 0;
		if (count(array_unique($projectsNames)) === count($projectsNames)) {
			foreach ($projectsNames as $projectName) {
				$report = new EstimatedReport($employee, $employeeEmail, $projectName, $dateFrom, $dateTo);
				$hours += $report->hours();
			}
		} else {
			echo '---> You have some project more than one time in "projects names" array!';
		}
		$this->assertEquals(6.75, $hours);
	}

	public function testProjectMilestoneId() {
		$projectName = 'sample-landpage-bootstrap-v3.3.6';
		$milestoneTitle = 'v1';

		$report = new Milestones($projectName, $milestoneTitle, '', true);
		$id = $report->milestoneId();
		$this->assertEquals(702738, $id);
	}

	public function testProjectMilestoneIssues() {
		$projectName = 'sample-landpage-bootstrap-v3.3.6';
		$milestoneTitle = 'v1';
		$expected = [];
		$data = json_decode('[{"id":15644582,"iid":4,"project_id":9267598,"title":"footer","description":"At least some paragraph or smtng to show in footer.","state":"closed","created_at":"2018-11-07T21:07:18.535Z","updated_at":"2018-12-21T23:24:09.652Z","closed_at":"2018-12-21T23:24:09.622Z","closed_by":{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"},"labels":["feature"],"milestone":{"id":702738,"iid":1,"project_id":9267598,"title":"v1","description":"Create simple web template page.","state":"active","created_at":"2018-11-07T20:58:24.925Z","updated_at":"2018-11-07T20:58:24.925Z","due_date":"2018-11-10","start_date":"2018-11-07","web_url":"https://gitlab.com/ravins/sample-landpage-bootstrap-v3.3.6/milestones/1"},"assignees":[{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"}],"author":{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"},"assignee":{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"},"user_notes_count":0,"upvotes":0,"downvotes":0,"due_date":"2018-11-09","confidential":false,"discussion_locked":null,"web_url":"https://gitlab.com/ravins/sample-landpage-bootstrap-v3.3.6/issues/4","time_stats":{"time_estimate":3600,"total_time_spent":0,"human_time_estimate":"1h","human_total_time_spent":null}},{"id":15644564,"iid":3,"project_id":9267598,"title":"Body","description":"Add some simple elements.","state":"closed","created_at":"2018-11-07T21:05:26.221Z","updated_at":"2018-12-21T23:24:44.884Z","closed_at":"2018-12-21T23:24:44.857Z","closed_by":{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"},"labels":["feature"],"milestone":{"id":702738,"iid":1,"project_id":9267598,"title":"v1","description":"Create simple web template page.","state":"active","created_at":"2018-11-07T20:58:24.925Z","updated_at":"2018-11-07T20:58:24.925Z","due_date":"2018-11-10","start_date":"2018-11-07","web_url":"https://gitlab.com/ravins/sample-landpage-bootstrap-v3.3.6/milestones/1"},"assignees":[{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"}],"author":{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"},"assignee":{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"},"user_notes_count":0,"upvotes":0,"downvotes":0,"due_date":"2018-11-09","confidential":false,"discussion_locked":null,"web_url":"https://gitlab.com/ravins/sample-landpage-bootstrap-v3.3.6/issues/3","time_stats":{"time_estimate":10800,"total_time_spent":0,"human_time_estimate":"3h","human_total_time_spent":null}},{"id":15644538,"iid":2,"project_id":9267598,"title":"Header","description":"Add Bootstrap init if not. Add header menu items.","state":"closed","created_at":"2018-11-07T21:02:52.291Z","updated_at":"2018-12-21T23:26:03.777Z","closed_at":"2018-12-21T23:26:03.731Z","closed_by":{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"},"labels":["feature"],"milestone":{"id":702738,"iid":1,"project_id":9267598,"title":"v1","description":"Create simple web template page.","state":"active","created_at":"2018-11-07T20:58:24.925Z","updated_at":"2018-11-07T20:58:24.925Z","due_date":"2018-11-10","start_date":"2018-11-07","web_url":"https://gitlab.com/ravins/sample-landpage-bootstrap-v3.3.6/milestones/1"},"assignees":[{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"}],"author":{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"},"assignee":{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"},"user_notes_count":0,"upvotes":0,"downvotes":0,"due_date":"2018-11-08","confidential":false,"discussion_locked":null,"web_url":"https://gitlab.com/ravins/sample-landpage-bootstrap-v3.3.6/issues/2","time_stats":{"time_estimate":6300,"total_time_spent":0,"human_time_estimate":"1h 45m","human_total_time_spent":null}},{"id":15644510,"iid":1,"project_id":9267598,"title":"Add index file","description":"Create simple structure of page layout.","state":"closed","created_at":"2018-11-07T21:01:17.321Z","updated_at":"2018-12-21T23:25:22.722Z","closed_at":"2018-12-21T23:25:22.678Z","closed_by":{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"},"labels":["feature"],"milestone":{"id":702738,"iid":1,"project_id":9267598,"title":"v1","description":"Create simple web template page.","state":"active","created_at":"2018-11-07T20:58:24.925Z","updated_at":"2018-11-07T20:58:24.925Z","due_date":"2018-11-10","start_date":"2018-11-07","web_url":"https://gitlab.com/ravins/sample-landpage-bootstrap-v3.3.6/milestones/1"},"assignees":[{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"}],"author":{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"},"assignee":{"id":1149983,"name":"Artūrs Rāviņš","username":"ravins116","state":"active","avatar_url":"https://assets.gitlab-static.net/uploads/-/system/user/avatar/1149983/avatar.png","web_url":"https://gitlab.com/ravins116"},"user_notes_count":0,"upvotes":0,"downvotes":0,"due_date":"2018-11-08","confidential":false,"discussion_locked":null,"web_url":"https://gitlab.com/ravins/sample-landpage-bootstrap-v3.3.6/issues/1","time_stats":{"time_estimate":3600,"total_time_spent":0,"human_time_estimate":"1h","human_total_time_spent":null}}]', true);
		foreach ($data as $issue) {
			$expected[] = json_decode(json_encode($issue), True);
		}
		$report = new MilestoneIssues($projectName, $milestoneTitle, '', true);
		$issues = $report->milestoneIssues();
		$this->assertEquals($expected, $issues);
	}
}