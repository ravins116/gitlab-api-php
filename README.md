# Gitlab API via PHP
Project contains PHP wrapper to be used with [GitLab API](https://docs.gitlab.com/ee/api/).
Created to run some group projects functions, but also can calculate effort to others projects.

## Installation
Download repository to your workstation and set phpunit tests to work with PHPUnit. Setup `Gitlab Configuration`.

### Gitlab Configuration

1. Go to `GitLab/Configuration.php-dist`.
2. Duplicate this file and refactor name without `-dist`.
3. Do not add new `GitLab/Configuration.php` file to VCS.
4. Go for your Gitlab [Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) in your Gitlab profile settings _(https://gitlab.com/profile/personal_access_tokens)_.
5. Copy your [Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) or create new _([description](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html))_ for next steps using.
6. Change [Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) variable in new `GitLab/Configuration.php` file.
7. Enjoy at least something.

```
// Change Token to your GitLab Personal access token
class Configuration
{
    const TOKEN = 'xxxxxxxxxxxx';
    const ISSUES_BUSINESS_LABELS = array('IP', '[IP]', 'Feature', 'Feature Set', 'Backlog Item', '[Backlog Item]');
}
```


### Gitlab FAQ

1. What if I check programmer hours and get errors? 
- You need to check Gitlab Configuration and set correct user full name and email address.